  // Task class
  function Task(data) {
    
    // Task data
    this.id         = data.id;
    this.content    = data.content;
    this.completed  = data.completed;
    this.deleted    = data.deleted_at;
    this.created    = data.created_at;
    
    // Task methods
    
    // Display task html
    this.display = function() {
      var self = this;
      return '<div class="input-group input-group-lg todo-item" task_id="' + self.id + '">\n' +
             '   <span class="input-group-btn">\n' +
             '     <button class="btn ' + (self.completed == 1 ? 'btn-success' : 'btn-default' ) + ' btn-lg complete" ' + ((self.completed == 1 || self.deleted)  ? 'disabled' : '' ) + ' title="' + (self.completed == 1 ? 'Completed at ' + self.deleted : 'Complete task' ) + '"><i class="glyphicon glyphicon-ok"></i></button>\n' +
             '   </span>\n' +
             '   <input class="edit form-control input-lg" value="' + self.content + '" readonly="readonly">\n' +
             '   <span class="input-group-btn">\n' +
             '     <button class="btn btn-default btn-lg remove" ' + (self.deleted ? 'disabled' : '' ) + ' title="' + (self.deleted ? 'Deleted at ' + self.deleted : 'Delete task' ) + '"><i class="glyphicon glyphicon-trash"></i></button>\n' +
             '   </span>\n' +
             '</div>';
    };
  }