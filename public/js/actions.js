$(document).ready(function() {
  
  var tasks = new Tasks();
  
  // Load tasks
  var load = function() { 
    $(".todo-items").html(""); // Clear contents
    tasks.load().then(
      function(response) {
        tasks.display(); // Display tasks
      }
    ); 
  }
  
  // Add click event listener for complete button
  $('body').on('click', 'button.complete', function(e) {
    e.preventDefault();
    
    var parent = $(this).parents(".todo-item");
    var id = parent.attr("task_id");
    
    tasks.complete(id).then(function(response) {
      parent.fadeOut("slow", function() {
        load();  
      });
    });
  });
  
  // Add click event listener for remove button
  $('body').on('click', 'button.remove', function(e) {
    e.preventDefault();
    
    var parent = $(this).parents(".todo-item");
    var id = parent.attr("task_id");
    
    tasks.remove(id).then(function(response) {
      parent.fadeOut("slow", function() {
        load();  
      });
    });
  });
  
  // Add click event listener for add button
  $('#add-new-task').submit('button.add', function(e) {
    e.preventDefault();
    
    var content = $("#add-item").val();
    
    if(content) {
    tasks.add(content).then(function(response) {
      load();
      $("#add-item").val('');
    });
    }
    else {
      alert("Please provide task content.")
    }
  });
  
  
  // Init
  load();
  
});