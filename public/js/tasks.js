  
  function Tasks(scope) {
    
    // Task data
    this.tasks       = new Array();
    this.completed   = new Array();
    this.deleted     = new Array();
    this.tasksEl     = $("#tasks .todo-items");
    this.deletedEl   = $("#deleted .todo-items");
    this.completedEl = $("#completed .todo-items");
    
    // Load tasks method
    this.load = function() {
      var self = this;
      self.tasks       = new Array();
      self.completed   = new Array();
      self.deleted     = new Array();
      
      return new Promise(function(resolve, reject) {
        // load task list from server
        $.get(
          "tasklist",
          function( data ) {
            $.each(data.tasks, function(i, task) {
              self.tasks.push(new Task(task));
            });
            $("#num_tasks").html(self.tasks.length);
            
            $.each(data.completed, function(i, task) {
              self.completed.push(new Task(task));
            });
            $("#num_completed").html(self.completed.length);
            
            $.each(data.deleted, function(i, task) {
              self.deleted.push(new Task(task));
            });
            $("#num_deleted").html(self.deleted.length);
          },
          "json" 
        )
        .done(function(response) {
          resolve(response);
        })
        .fail(function(response) {
          reject(Error("Something went wrong!"));
        });
      });
    };
    
    // Display tasks
    this.display = function() {
      var self = this;
      
      $.each(self.tasks, function(i, task) {
        self.tasksEl.append($.parseHTML(task.display()));
      });
      $.each(self.completed, function(i, task) {
        self.completedEl.append($.parseHTML(task.display()));
      });
      $.each(self.deleted, function(i, task) {
        self.deletedEl.append($.parseHTML(task.display()));
      });
    };
    
    // Complete task
    this.add = function(content) {
      var self = this;
      content = content || null;

      return new Promise(function(resolve, reject) {
        // load task list from server
        if(content) {
          $.post("add", {content: content})
          .done(function(response) {
            resolve(response);
          })
          .fail(function(response) {
            reject(Error("Something went wrong!"));
          });
        }
        else {
          reject(Error("Please provide task content!"));
        }
      });
    };
    
    // Complete task
    this.complete = function(id) {
      var self = this;
      id = id || null;
      
      return new Promise(function(resolve, reject) {
        // load task list from server
        if(id) {
          $.get("complete/" + id)
          .done(function(response) {
            resolve(response);
          })
          .fail(function(response) {
            reject(Error("Something went wrong!"));
          });
        }
        else {
          reject(Error("Please provide task ID!"));
        }
      });
    };
    
    // Remove task
    this.remove = function(id) {
      var self = this;
      id = id || null;
      
      return new Promise(function(resolve, reject) {
        // load task list from server
        if(id) {
          $.get("remove/" + id)
          .done(function(response) {
            resolve(response);
          })
          .fail(function(response) {
            reject(Error("Something went wrong!"));
          });
        }
        else {
          reject(Error("Please provide task ID!"));
        }
      });
    };
  }