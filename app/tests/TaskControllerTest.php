<?php
 
use Mockery;
use TestCase;
 
class TaskControllerTest extends TestCase {
  
  public function __construct()
  {
    $this->mock = Mockery::mock('Eloquent', 'Task');
  }  
  
  public function tearDown()
  {
    Mockery::close();
  }
  
  // What happens if empty input
  public function testCreateEmptyPost()
  {
    $response = $this->call('POST', 'add', array("content" => ""));
    
    $data = (object) array(
      "error" => "Could not create task!", 500
    );
    
    $this->assertEquals($data, $response->getData());  
  }
  
  // What happens if correct input
  public function testCreatePost()
  {
    $response = $this->call('POST', 'add', array("content" => "Test task"));
    
    $data = (object) array(
      "error" => false, 200
    );
    
    $this->assertEquals($data, $response->getData());  
  }

}