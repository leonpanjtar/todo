@extends('layout')

@section('content')
    <h1>ToDo Demo</h1>
    
    <div class="todo-add">
      <div class="input-group input-group-lg">
        <span class="input-group-btn">
          <button class="btn btn-success btn-lg"><i class="glyphicon glyphicon-chevron-right"></i></button>
        </span>
        <form name="add-new-task" id="add-new-task">          
          <input class="edit form-control input-lg" placeholder="Enter task content and click the + button" id="add-item">
        </form>
        <span class="input-group-btn">
          <button class="btn btn-default btn-lg add" type="submit"><i class="glyphicon glyphicon-plus"></i></button>
        </span>
      </div>
    </div>
    
    <!-- Nav tabs -->
    <ul class="nav nav-pills" role="tablist">
      <li class="active"><a href="#tasks" role="tab" data-toggle="tab"><span id="num_tasks" class="badge pull-right">42</span>Tasks</a></li>
      <li><a href="#completed" role="tab" data-toggle="tab"><span id="num_completed" class="badge pull-right">42</span>Completed</a></li>
      <li><a href="#deleted" role="tab" data-toggle="tab"><span id="num_deleted" class="badge pull-right">42</span>Deleted</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="tasks">
        <div class="todo-items"></div>
      </div>
      <div class="tab-pane" id="completed">
        <div class="todo-items"></div>
      </div>
      <div class="tab-pane" id="deleted">
        <div class="todo-items"></div>
      </div>
    </div>

@stop