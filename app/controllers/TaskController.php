<?php

class TaskController extends \BaseController {

  protected $task;
 
  public function __construct(Task $task)
  {
      $this->task = $task;
  }

	/**
	 * Display the main layout for tasks.
	 *
	 * @return Response
	 */
	public function index()
	{
      return View::make('tasks');
	}

	/**
	 * Return listing of tasks.
	 *
	 * @return Response
	 */
	public function listing()
	{
      $tasks = $this->task->where('completed', '=', 0)->get();
      $deleted = $this->task->onlyTrashed()->get();
      $completed = $this->task->where('completed', '=', 1)->get();
 
      return Response::json(array(
        'error' => false,
        'tasks' => $tasks->toArray(),
        'deleted' => $deleted->toArray(),
        'completed' => $completed->toArray()
        ),
        200
      );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    $task = array(
      "content" =>  Input::get('content', 'Sample Task')
    );
    
    if($task["content"] != "" && $this->task->create($task)) {
      return Response::json(array('error' => false, 200));
    } 
    else {
      return Response::json(array('error' => "Could not create task!", 500));
    }
	}


	/**
	 * Set complete status of task.
	 *
	 * @return Response
	 */
	public function complete( $id )
	{
    $params = array( "completed" => 1 );
  
    if($this->task->find($id)->update($params)) {
      return Response::json(array('error' => false, 200));
    } 
    else {
      return Response::json(array('error' => "Could not complete task!", 500));
    }
	}

	/**
	 * Remove the specified task
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy( $id )
	{
    $task = $this->task->find($id);
    if($task->delete()) {
      return Response::json(array('error' => false, 200));
    } 
    else {
      return Response::json(array('error' => "Could not delete task!", 500));
    }
	}


}
