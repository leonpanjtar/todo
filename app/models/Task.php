<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Task extends Eloquent {
    
    use SoftDeletingTrait;
    
    protected $dates = ['deleted_at'];
    protected $fillable = array('content', 'completed');
  
}