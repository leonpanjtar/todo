<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::pattern('id', '[0-9]+');

Route::get('/', 'TaskController@index');

// Get task list
Route::get('tasklist', 'TaskController@listing');

// Handle a new task POST
Route::post('add', 'TaskController@create');

// Complete taks
Route::get('complete/{id}', 'TaskController@complete');

// Delete taks
Route::get('remove/{id}', 'TaskController@destroy');
